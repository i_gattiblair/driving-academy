﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class ClientScheduleAppointment : Form
    {
        
        public ClientScheduleAppointment(string username, string competence)
        {
            InitializeComponent();
            
            //changes 'Your Username is:' label to logged in Username
            clientUsernameLabel.Text = username;
            comptLvl.Text = competence;


            //for loop for the scheduling hours
            for (int beginningOfHour = 7; beginningOfHour < 20; beginningOfHour++)
            {
                int endOfHour = beginningOfHour + 1;
                string beginningOfHourToString = beginningOfHour.ToString();
                string endOfHourToString = endOfHour.ToString();
                HourSchedule.Items.Add(beginningOfHourToString + ":00-" + endOfHourToString + ":00");
            }

            //for loop for the scheduling dates
            int dayOfWeekIndex = 0;
            for (int day = 2; day < 32; day ++)
            {
                string dayToString = day.ToString();
                string[] dayOfWeek = new string[] { "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY" };


               
                DateSchedule.Items.Add("2017-01-" + dayToString + "  " + dayOfWeek[dayOfWeekIndex]);

                dayOfWeekIndex++;
                    if(dayOfWeekIndex == 6)
                    {
                        dayOfWeekIndex = 0;
                        day++;
                    }
            }

            //<SUMMARY>
            //Populate 'Choose Your Instructor' dropdown box with the instructors from TABLE instructors
            List<String> columnData = new List<String>();

            SQL.selectQuery("SELECT fname, sname FROM instructor");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    columnData.Add(SQL.read[0].ToString());
                    
                }
            }
            
            InstructorChoose.DataSource = columnData;
            //</SUMMARY>

            

        }

        private void ClientAppointmentCheck_Click(object sender, EventArgs e)
        {
            //sends QUERY to 'appointment' TABLE in server
            string hourSelected = HourSchedule.Text;
            string dateSelected = DateSchedule.Text;
            string instructorSelected = InstructorChoose.Text;
            string username = clientUsernameLabel.Text;
            string competence = comptLvl.Text;
            bool appointmentMade = false;
            List<String> num = new List<String>();


            SQL.selectQuery("SELECT * FROM appointment");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (hourSelected.Equals(SQL.read[1].ToString()) && dateSelected.Equals(SQL.read[2].ToString()) && instructorSelected.Equals(SQL.read[5].ToString()))
                    {
                        MessageBox.Show("Sorry, instructor is already working at this time and date. Pick another.");
                        appointmentMade = false;
                        break;
                    }

                    else
                    {
                        appointmentMade = true;
                    }

                    num.Add(SQL.read[0].ToString());

                    
                }

            }

            if (num.Count < 1)
            {
                appointmentMade = true;
            }

            if (appointmentMade == true)
            {                                                          
                SQL.executeQuery("INSERT INTO appointment VALUES ('" + hourSelected + "','" + dateSelected + "','" + username + "','" + competence + "','" + instructorSelected + "','Not Yet' )");

                MessageBox.Show("You have made an appointment at HOUR: '" + hourSelected + "', DATE: '" + dateSelected + "' with '" + instructorSelected + "'.");
                
                ///////////////////////////////////
                List<String> appointmentData = new List<String>();
                List<String> clientName = new List<String>();
                List<String> appID = new List<String>();
                var hour = HourSchedule.Text;
                var date = DateSchedule.Text;
                var instName = InstructorChoose.Text;
                
                SQL.selectQuery("SELECT id, time, date, client_username FROM appointment WHERE time= '" + hour + "' AND date = '" + date + "' AND instructor_username='" + instName + "'");

                
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {

                        appointmentData.Add("Time: " + SQL.read[1].ToString() + "  || Date: " + SQL.read[2].ToString());
                        clientName.Add(SQL.read[3].ToString());
                        appID.Add(SQL.read[0].ToString());
                    }
                }


                if (appID.Count >= 0)
                {
                    string results = string.Join(",", appointmentData.ToArray());
                    string cliName = string.Join(",", clientName.ToArray());
                    string ai = string.Join(",", appID.ToArray());

                    string[] lines = { "Your Appointment Is: " + results + " || INSTRUCTOR: " + instName };

                    System.IO.File.WriteAllLines(@"C:\Users\9935621\Desktop\DIA\Appointments\" + ai.ToString() + "_" + cliName.ToString() + "_appointment" + ".txt", lines);

                    


                    MessageBox.Show("An email has been sent to with information of your appointment");
                }

                

            }

                        
        }

        private void ReturnLogIn_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            LoginPage login = new LoginPage();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }
    }
}
