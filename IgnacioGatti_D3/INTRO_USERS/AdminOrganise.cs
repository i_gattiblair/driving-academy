﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class AdminOrganise : Form
    {
        public AdminOrganise()
        {
            InitializeComponent();
            myReload();
            carAppointLoad();
            
        }

        private void LoginPage_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            LoginPage login = new LoginPage();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        private void myReload()
        {
            
            List<String> workingHoursData = new List<String>();

            SQL.selectQuery("SELECT * FROM workingHours");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    workingHoursData.Add("INSTRUCTOR:  " + SQL.read[1].ToString() + "  || DAY OF WEEK:  " + SQL.read[2].ToString() + "  || CAR:  " + SQL.read[3].ToString());
                    
                }
            }

            InstructorsAssignedHoursList.DataSource = workingHoursData;

            //////////////////////////////////////////////////


            List<String> staff = new List<String>();

            SQL.selectQuery("SELECT instructor_username FROM instructor UNION SELECT DISTINCT username FROM admin");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    staff.Add(SQL.read[0].ToString());

                }
            }

            staffList.DataSource = staff;

            var selstaff = staff[0].ToString();

            
            removeStaff.Text = "Remove: \"" + selstaff + "\"";

            /////////////////////////////////////////////

            List<String> clientComplete = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment app, type tp WHERE seen = 'Completed' AND app.competence = tp.name");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    clientComplete.Add("ID: " + SQL.read[0].ToString() + " || TIME:  " + SQL.read[1].ToString() + "  || DATE: " + SQL.read[2].ToString() + "  || CLIENT:  " + SQL.read[3].ToString() + "  || COMPETENCE:  " + SQL.read[4].ToString() + "  || INSTRUCTOR:  " + SQL.read[5].ToString() + "  || LEVEL:  " + SQL.read[7].ToString() + "  COST:  " + SQL.read[8].ToString());
                                       
                    
                }
            }

            ClientAppointmentComplete.DataSource = clientComplete;
        }//END OF myReload()

        private void AssignInstructorHours1_Click(object sender, EventArgs e)
        {
            string InstructorSelected = InstructorList1.Text;
            string DaysWorkingSelected = DaysWorking1.Text;
            string CarsListSelected = CarsList1.Text;
            bool CarAvailable = false;
            List<String> num = new List<String>();

            SQL.selectQuery("SELECT * FROM workingHours");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (DaysWorkingSelected.Equals(SQL.read[2].ToString()) && CarsListSelected.Equals(SQL.read[3].ToString()))
                    {
                        MessageBox.Show("Sorry, this car is already in use at this time");
                        CarAvailable = false;
                        break;
                    }

                    else
                    {
                        CarAvailable = true;
                    }

                    num.Add(SQL.read[0].ToString());
                }

            }

            if (num.Count < 1)
            {
                CarAvailable = true;
            }

            if (CarAvailable == true)
            {
                SQL.executeQuery("INSERT INTO workingHours VALUES('" + InstructorSelected + "','" + DaysWorkingSelected + "','" + CarsListSelected + "')");
                
            }

            myReload();
        }

        private void AssignInstructorHours2_Click(object sender, EventArgs e)
        {
            string InstructorSelected = InstructorList2.Text;
            string DaysWorkingSelected = DaysWorking2.Text;
            string CarsListSelected = CarsList2.Text;
            bool CarAvailable = false;
            List<String> num = new List<String>();
            SQL.selectQuery("SELECT * FROM workingHours");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (DaysWorkingSelected.Equals(SQL.read[2].ToString()) && CarsListSelected.Equals(SQL.read[3].ToString()))
                    {
                        MessageBox.Show("Sorry, this car is already in use at this time");
                        CarAvailable = false;
                        break;
                    }

                    else
                    {
                        CarAvailable = true;
                    }

                    num.Add(SQL.read[0].ToString());

                }
                    
            }

            if (num.Count < 1)
            {
                CarAvailable = true;
            }

            if (CarAvailable == true)
            {
                SQL.executeQuery("INSERT INTO workingHours VALUES('" + InstructorSelected + "','" + DaysWorkingSelected + "','" + CarsListSelected + "')");
                
            }

            
            myReload();
        }

        private void AssignInstructorHours3_Click(object sender, EventArgs e)
        {
            string InstructorSelected = InstructorList3.Text;
            string DaysWorkingSelected = DaysWorking3.Text;
            string CarsListSelected = CarsList3.Text;
            bool CarAvailable = false;
            List<String> num = new List<String>();

            SQL.selectQuery("SELECT * FROM workingHours");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (DaysWorkingSelected.Equals(SQL.read[2].ToString()) && CarsListSelected.Equals(SQL.read[3].ToString()))
                    {
                        MessageBox.Show("Sorry, this car is already in use at this time");
                        CarAvailable = false;
                        break;
                    }

                    else
                    {
                        CarAvailable = true;
                    }

                    num.Add(SQL.read[0].ToString());
                }

            }

            if (num.Count < 1)
            {
                CarAvailable = true;
            }

            if (CarAvailable == true)
            {
                SQL.executeQuery("INSERT INTO workingHours VALUES('" + InstructorSelected + "','" + DaysWorkingSelected + "','" + CarsListSelected + "')");
                
            }

            myReload();
        }

        private void AssignAllInstructorHours_Click(object sender, EventArgs e)
        {
            string InstructorSelected1 = InstructorList1.Text;
            string InstructorSelected2 = InstructorList2.Text;
            string InstructorSelected3 = InstructorList3.Text;

            string DaysWorkingSelected1 = DaysWorking1.Text;
            string DaysWorkingSelected2 = DaysWorking2.Text;
            string DaysWorkingSelected3 = DaysWorking3.Text;

            string CarsListSelected1 = CarsList1.Text;
            string CarsListSelected2 = CarsList2.Text;
            string CarsListSelected3 = CarsList3.Text;

            SQL.executeQuery("INSERT INTO workingHours VALUES('" + InstructorSelected1 + "','" + DaysWorkingSelected1 + "','" + CarsListSelected1 + "')");
            SQL.executeQuery("INSERT INTO workingHours VALUES('" + InstructorSelected2 + "','" + DaysWorkingSelected2 + "','" + CarsListSelected2 + "')");
            SQL.executeQuery("INSERT INTO workingHours VALUES('" + InstructorSelected3 + "','" + DaysWorkingSelected3 + "','" + CarsListSelected3 + "')");

            myReload();
        }

        private void ShowOnlyBtn_Click(object sender, EventArgs e)
        {
            string showOnlySelected = ShowOnlyList.Text;

            List<String> workingHoursData = new List<String>();

            SQL.selectQuery("SELECT * FROM workingHours WHERE instructor_username='" + showOnlySelected + "'");

            
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    workingHoursData.Add("INSTRUCTOR:  " + SQL.read[1].ToString() + "  || DAY OF WEEK:  " + SQL.read[2].ToString() + "  || CAR:  " + SQL.read[3].ToString());

                }
            }

            
            InstructorsAssignedHoursList.DataSource = workingHoursData;

            
        }

        private void ShowEveryone_Click(object sender, EventArgs e)
        {
            myReload();
        }

        private void AssignAllInstructorHours_Click_1(object sender, EventArgs e)
        {

        }

        private void staffList_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<String> staff = new List<String>();

            SQL.selectQuery("SELECT instructor_username FROM instructor UNION SELECT username FROM admin");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    staff.Add(SQL.read[0].ToString());

                }
            }

            var selstaff = staff[staffList.SelectedIndex].ToString();

            

            removeStaff.Text = "Remove: \"" + selstaff + "\"";

            
            
        }

        private void removeStaff_Click(object sender, EventArgs e)
        {

            List<String> staff = new List<String>();

            SQL.selectQuery("SELECT instructor_username FROM instructor UNION SELECT DISTINCT username FROM admin");

            var selstaff = "";

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    staff.Add(SQL.read[0].ToString());
                    
                }
            }

            if (staff.Count < 1)
            {
                //nothing
            }

            else
            {
                selstaff = staff[staffList.SelectedIndex].ToString();
            }
            


            DialogResult dialogResult = MessageBox.Show("You sure you want to Delete \"" + selstaff + "\"", "Delete Staff Member", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SQL.executeQuery("DELETE FROM instructor where instructor_username = '" + selstaff + "'");
                SQL.executeQuery("DELETE FROM admin where username = '" + selstaff + "'");

                MessageBox.Show("Successfully Removed: \"" + selstaff + "\" from the Database.");
                
            }
            else if (dialogResult == DialogResult.No)
            {
                //do nothing
            }

            myReload();
        }

        private void showStatistics_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            Statistics login = new Statistics();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        private void carAppointLoad()
        {
            //Populates names of Instructors
            List<String> columnDataInst1 = new List<String>();
            List<String> columnDataInst2 = new List<String>();
            List<String> columnDataInst3 = new List<String>();
            List<String> columnDataInst4 = new List<String>();

            SQL.selectQuery("SELECT instructor_username FROM instructor");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    columnDataInst1.Add(SQL.read[0].ToString());
                    columnDataInst2.Add(SQL.read[0].ToString());
                    columnDataInst3.Add(SQL.read[0].ToString());
                    columnDataInst4.Add(SQL.read[0].ToString());

                }
            }

            InstructorList1.DataSource = columnDataInst1;
            InstructorList1.SelectedIndex = 0;

            InstructorList2.DataSource = columnDataInst2;
            InstructorList2.SelectedIndex = 1;


            InstructorList3.DataSource = columnDataInst3;
            InstructorList3.SelectedIndex = 2;

            ShowOnlyList.DataSource = columnDataInst4;

            //

            string[] dayOfWeek1 = new string[] { "MONDAY - 13 Hrs", "TUESDAY - 13 Hrs", "WEDNESDAY - 13 Hrs", "THURSDAY - 13 Hrs", "FRIDAY - 13 Hrs", "SATURDAY - 13 Hrs" };
            string[] dayOfWeek2 = new string[] { "MONDAY - 13 Hrs", "TUESDAY - 13 Hrs", "WEDNESDAY - 13 Hrs", "THURSDAY - 13 Hrs", "FRIDAY - 13 Hrs", "SATURDAY - 13 Hrs" };
            string[] dayOfWeek3 = new string[] { "MONDAY - 13 Hrs", "TUESDAY - 13 Hrs", "WEDNESDAY - 13 Hrs", "THURSDAY - 13 Hrs", "FRIDAY - 13 Hrs", "SATURDAY - 13 Hrs" };


            DaysWorking1.DataSource = dayOfWeek1;
            DaysWorking2.DataSource = dayOfWeek2;
            DaysWorking3.DataSource = dayOfWeek3;


            //Populates car license numbers
            List<String> columnDataCars1 = new List<String>();
            List<String> columnDataCars2 = new List<String>();
            List<String> columnDataCars3 = new List<String>();

            SQL.selectQuery("SELECT * FROM cars");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    columnDataCars1.Add(SQL.read[0].ToString() + " - " + SQL.read[1].ToString());
                    columnDataCars2.Add(SQL.read[0].ToString() + " - " + SQL.read[1].ToString());
                    columnDataCars3.Add(SQL.read[0].ToString() + " - " + SQL.read[1].ToString());

                }
            }

            CarsList1.DataSource = columnDataCars1;


            CarsList2.DataSource = columnDataCars2;
            CarsList2.SelectedIndex = 1;

            CarsList3.DataSource = columnDataCars3;
            CarsList3.SelectedIndex = 2;
        }

        private void sendBill_Click(object sender, EventArgs e)
        {
            List<String> selectedID = new List<String>();
            

            SQL.selectQuery("SELECT * FROM appointment app, type tp WHERE seen = 'Completed' AND app.competence = tp.name");

            string selID = null;

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    selectedID.Add(SQL.read[0].ToString());
                    
                }
            }

            selID = selectedID[ClientAppointmentComplete.SelectedIndex].ToString();
            ////////////////////
            List<String> appointmentData = new List<String>();
            List<String> clientName = new List<String>();
            List<String> appID = new List<String>();
            SQL.selectQuery("SELECT * FROM appointment app, type tp WHERE seen = 'Completed' AND app.competence = tp.name AND id ='" + selID + "'" );
            

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    appointmentData.Add("Time: " + SQL.read[1].ToString() + "  || Date: " + SQL.read[2].ToString() + "  || Instructor Name: " + SQL.read[5].ToString() + " || LEVEL: "+ SQL.read[7].ToString() + " || COST: $" + SQL.read[8].ToString() + " || HOURS: " + SQL.read[9].ToString());
                    clientName.Add(SQL.read[3].ToString());
                    appID.Add(SQL.read[0].ToString());
                }
            }

            if (selID != "")
            {
                string results = string.Join(",", appointmentData.ToArray());
                string cliName = string.Join(",", clientName.ToArray());
                string ai = string.Join(",", appID.ToArray());

                string[] lines = { "Your Bill For Your Appointment Is: " + results };

                

                if (ai != "")
                {
                    System.IO.File.WriteAllLines(@"C:\Users\9935621\Desktop\DIA\Bills\" + ai.ToString() + "_" + cliName.ToString() + "_bill" + ".txt", lines);
                    MessageBox.Show("The Bill Has Been Sent");
                }
                
            }

            
        }

        private void clientPaid_Click(object sender, EventArgs e)
        {
            List<String> selectedID = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment WHERE seen = 'Completed'");

            var selID = "";

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    selectedID.Add(SQL.read[0].ToString());
                    selID = selectedID[ClientAppointmentComplete.SelectedIndex].ToString();
                }
            }

            

            SQL.executeQuery("UPDATE appointment SET seen = 'Paid' WHERE seen = 'Completed' AND  id = " + selID);

            

            myReload();
        }

        private void RemoveCarAppointment_Click(object sender, EventArgs e)
        {
            List<String> carAppID = new List<String>();

            SQL.selectQuery("SELECT * FROM workingHours");
            
            string selCarAppID = null;

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    carAppID.Add(SQL.read[0].ToString());
                    
                }
            }

            if (carAppID.Count < 1)
            {
                //nothing
            }

            else
            {
                selCarAppID = carAppID[staffList.SelectedIndex].ToString();

                SQL.executeQuery("DELETE FROM workingHours WHERE wid='" + selCarAppID + "'");
            }



            myReload();
        }
    }
}

