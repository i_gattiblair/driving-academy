﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class Statistics : Form
    {
        public Statistics()
        {
            InitializeComponent();
            confirmedAppointment();
            CompletedAppointments();
            paidAppointments();

        }

        private void ReturnLogin_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            AdminOrganise login = new AdminOrganise();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        private void confirmedAppointment()
        {
            //List that is gonna be populated with SQL data
            List<String> InstName = new List<String>();
            List<String> InstStat = new List<String>();

            //SQL Query
            SQL.selectQuery("SELECT DISTINCT instructor_username, COUNT(instructor_username) FROM appointment WHERE seen = 'Confirmed' GROUP BY instructor_username");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    InstName.Add(SQL.read[0].ToString());
                    InstStat.Add(SQL.read[1].ToString());

                }
            }


            //Dynamically populates ListBox with SQL data.
            //ConfirmedAppointments = ListBox Name
            int i = 0;
            for (i = 0; i < InstName.Count(); i++)
            {
                ConfirmedAppointments.Series.Add("");
                ConfirmedAppointments.Series[i].Name = InstName[i].ToString();
                ConfirmedAppointments.Series[i].Points.AddY(Convert.ToInt32(InstStat[i]));
            }
        }

        private void CompletedAppointments()
        {
            List<String> InstName = new List<String>();
            List<String> InstStat = new List<String>();

            SQL.selectQuery("SELECT DISTINCT instructor_username, COUNT(instructor_username) FROM appointment WHERE seen = 'Completed' GROUP BY instructor_username");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    InstName.Add(SQL.read[0].ToString());
                    InstStat.Add(SQL.read[1].ToString());

                }
            }

            //MessageBox.Show(InstName.Count().ToString());

            int i = 0;
            for (i = 0; i < InstName.Count(); i++)
            {
                InstPerClient.Series.Add("");
                InstPerClient.Series[i].Name = InstName[i].ToString();
                InstPerClient.Series[i].Points.AddY(Convert.ToInt32(InstStat[i]));
            }
        }


        private void paidAppointments()
        {
            List<String> InstName = new List<String>();
            List<String> InstStat = new List<String>();

            SQL.selectQuery("SELECT DISTINCT instructor_username, COUNT(instructor_username) FROM appointment WHERE seen = 'Paid' GROUP BY instructor_username");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    InstName.Add(SQL.read[0].ToString());
                    InstStat.Add(SQL.read[1].ToString());

                }
            }

            //MessageBox.Show(InstName.Count().ToString());

            int i = 0;
            for (i = 0; i < InstName.Count(); i++)
            {
                PaidAppointments.Series.Add("");
                PaidAppointments.Series[i].Name = InstName[i].ToString();
                PaidAppointments.Series[i].Points.AddY(Convert.ToInt32(InstStat[i]));
            }
        }
    }
}
