﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class InstructorAvailability : Form
    {
        public string fn = "";
        
        public InstructorAvailability(string username, string firstname)
        {
            InitializeComponent();
            fn = firstname;
            //changes 'Your Username is:' label to logged in Username
            InstructorUsernameLabel.Text = username;
            
            
            myReload();
            bookAppointment();



        }

        private void ReturnLogInPage_Click(object sender, EventArgs e)
        {
            //hides this form currently on
            Hide();
            //is the login page as a new object               
            LoginPage login = new LoginPage();
            //Shows the login page window
            login.ShowDialog();
            //closes the current open windows so its only the new one showing
            this.Close();
        }

        
               

        private void myReload()
        {

            List<String> confirmedAppointment = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment WHERE instructor_username='" + fn + "' AND seen = 'Confirmed' ");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    confirmedAppointment.Add("ID: " + SQL.read[0].ToString() + "  ||  Time: " + SQL.read[1].ToString() + "  || Date: " + SQL.read[2].ToString() + "  || Client Username: " + SQL.read[3].ToString() + "|| Competence: " + SQL.read[4].ToString() + " || Seen: " + SQL.read[6].ToString());

                }
            }

            ConfirmedAppointment.DataSource = confirmedAppointment;

            
            List<String> appointmentData = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment WHERE instructor_username='" + fn + "' AND seen != 'Completed' AND seen != 'Paid'");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    appointmentData.Add("ID: " + SQL.read[0].ToString() + "  ||  Time: " + SQL.read[1].ToString() + "  || Date: " + SQL.read[2].ToString() + "  || Client Username: " + SQL.read[3].ToString() + "|| Competence: " + SQL.read[4].ToString() + " || Seen: " + SQL.read[6].ToString());

                }
            }

            BookedAppointmentList.DataSource = appointmentData;
            //</SUMMARY>
        }



        private void confirmAppointment_Click(object sender, EventArgs e)
        {
            List<String> selectedID = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment WHERE instructor_username='" + fn + "' AND seen != 'Completed' AND seen != 'Paid'");

            string selID = null;

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    selectedID.Add(SQL.read[0].ToString());

                }
            }

            if (selectedID.Count < 1)
            {
                //nothing
            }

            else
            {
                
                selID = selectedID[BookedAppointmentList.SelectedIndex].ToString();

                SQL.executeQuery("UPDATE appointment SET seen = 'Confirmed' WHERE seen = 'Not Yet' AND  id = " + selID);
            }
            
            
          
            
            myReload();

            
        }

        private void BookedAppointmentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<String> selectedUname = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment WHERE instructor_username='" + fn + "' AND seen != 'Completed' AND seen != 'Paid'");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    selectedUname.Add(SQL.read[3].ToString());

                }
            }

            var selUname = selectedUname[BookedAppointmentList.SelectedIndex].ToString();

            confirmAppointment.Text = "Confirm Appointment With: \"" + selUname + "\"";
            completeAppointment.Text = "Complete Appointment With: \"" + selUname + "\"";
            
        }

        private void completeAppointment_Click(object sender, EventArgs e)
        {
            List<String> selectedID = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment WHERE instructor_username='" + fn + "' AND seen != 'Completed' AND seen != 'Paid'");

            string selID = null;
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    selectedID.Add(SQL.read[0].ToString());

                }
            }


            if (selectedID.Count < 1)
            {
                //nothing
            }

            else
            {
                selID = selectedID[BookedAppointmentList.SelectedIndex].ToString();

                SQL.executeQuery("UPDATE appointment SET seen = 'Completed' WHERE seen = 'Confirmed' AND  id = " + selID);
            }

            

            myReload();
        }

        private void bookAppointment()
        {
            List<String> selectedUname = new List<String>();
            List<String> num = new List<String>();
            SQL.selectQuery("SELECT * FROM appointment WHERE instructor_username='" + fn + "'");

            var selUname = "";

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {

                    selectedUname.Add(SQL.read[3].ToString());
                    num.Add(SQL.read[0].ToString());
                }

                
            }

            if (num.Count < 1)
            {
                //nothing
            }

            else
            {
                selUname = selectedUname[BookedAppointmentList.SelectedIndex].ToString();
            }

            confirmAppointment.Text = "Confirm Appointment With: \"" + selUname + "\"";
            completeAppointment.Text = "Complete Appointment With: \"" + selUname + "\"";
        }
    }
}
