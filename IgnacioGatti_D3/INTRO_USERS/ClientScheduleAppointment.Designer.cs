﻿namespace INTRO_USERS
{
    partial class ClientScheduleAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientUsernameLabel = new System.Windows.Forms.Label();
            this.HourSchedule = new System.Windows.Forms.ComboBox();
            this.DateSchedule = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ClientAppointmentCheck = new System.Windows.Forms.Button();
            this.InstructorChoose = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ReturnLogIn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comptLvl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // clientUsernameLabel
            // 
            this.clientUsernameLabel.AutoSize = true;
            this.clientUsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientUsernameLabel.Location = new System.Drawing.Point(223, 43);
            this.clientUsernameLabel.Name = "clientUsernameLabel";
            this.clientUsernameLabel.Size = new System.Drawing.Size(83, 20);
            this.clientUsernameLabel.TabIndex = 22;
            this.clientUsernameLabel.Text = "Username";
            this.clientUsernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HourSchedule
            // 
            this.HourSchedule.FormattingEnabled = true;
            this.HourSchedule.Location = new System.Drawing.Point(42, 158);
            this.HourSchedule.Name = "HourSchedule";
            this.HourSchedule.Size = new System.Drawing.Size(121, 21);
            this.HourSchedule.TabIndex = 23;
            // 
            // DateSchedule
            // 
            this.DateSchedule.FormattingEnabled = true;
            this.DateSchedule.Location = new System.Drawing.Point(227, 158);
            this.DateSchedule.Name = "DateSchedule";
            this.DateSchedule.Size = new System.Drawing.Size(173, 21);
            this.DateSchedule.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 25;
            this.label1.Text = "Hour: (24hr)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(223, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 20);
            this.label3.TabIndex = 26;
            this.label3.Text = "Date (YYYY/MM/DD)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ClientAppointmentCheck
            // 
            this.ClientAppointmentCheck.Location = new System.Drawing.Point(42, 319);
            this.ClientAppointmentCheck.Name = "ClientAppointmentCheck";
            this.ClientAppointmentCheck.Size = new System.Drawing.Size(121, 57);
            this.ClientAppointmentCheck.TabIndex = 27;
            this.ClientAppointmentCheck.Text = "Done";
            this.ClientAppointmentCheck.UseVisualStyleBackColor = true;
            this.ClientAppointmentCheck.Click += new System.EventHandler(this.ClientAppointmentCheck_Click);
            // 
            // InstructorChoose
            // 
            this.InstructorChoose.FormattingEnabled = true;
            this.InstructorChoose.Location = new System.Drawing.Point(42, 247);
            this.InstructorChoose.Name = "InstructorChoose";
            this.InstructorChoose.Size = new System.Drawing.Size(121, 21);
            this.InstructorChoose.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Choose Your Instructor:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 20);
            this.label4.TabIndex = 30;
            this.label4.Text = "Your Username Is:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ReturnLogIn
            // 
            this.ReturnLogIn.Location = new System.Drawing.Point(42, 402);
            this.ReturnLogIn.Name = "ReturnLogIn";
            this.ReturnLogIn.Size = new System.Drawing.Size(121, 51);
            this.ReturnLogIn.TabIndex = 31;
            this.ReturnLogIn.Text = "Log In Page";
            this.ReturnLogIn.UseVisualStyleBackColor = true;
            this.ReturnLogIn.Click += new System.EventHandler(this.ReturnLogIn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(38, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Competence:";
            // 
            // comptLvl
            // 
            this.comptLvl.AutoSize = true;
            this.comptLvl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comptLvl.Location = new System.Drawing.Point(227, 90);
            this.comptLvl.Name = "comptLvl";
            this.comptLvl.Size = new System.Drawing.Size(46, 20);
            this.comptLvl.TabIndex = 33;
            this.comptLvl.Text = "Level";
            // 
            // ClientScheduleAppointment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 477);
            this.Controls.Add(this.comptLvl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ReturnLogIn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.InstructorChoose);
            this.Controls.Add(this.ClientAppointmentCheck);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateSchedule);
            this.Controls.Add(this.HourSchedule);
            this.Controls.Add(this.clientUsernameLabel);
            this.Name = "ClientScheduleAppointment";
            this.Text = "ClientScheduleAppointment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label clientUsernameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ClientAppointmentCheck;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ReturnLogIn;
        public System.Windows.Forms.ComboBox InstructorChoose;
        public System.Windows.Forms.ComboBox HourSchedule;
        public System.Windows.Forms.ComboBox DateSchedule;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label comptLvl;
    }
}