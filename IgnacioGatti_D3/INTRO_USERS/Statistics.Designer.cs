﻿namespace INTRO_USERS
{
    partial class Statistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.InstPerClient = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.ReturnLogin = new System.Windows.Forms.Button();
            this.ConfirmedAppointments = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.PaidAppointments = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.InstPerClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmedAppointments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidAppointments)).BeginInit();
            this.SuspendLayout();
            // 
            // InstPerClient
            // 
            chartArea1.Name = "ChartArea1";
            this.InstPerClient.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.InstPerClient.Legends.Add(legend1);
            this.InstPerClient.Location = new System.Drawing.Point(469, 91);
            this.InstPerClient.Name = "InstPerClient";
            this.InstPerClient.Size = new System.Drawing.Size(350, 282);
            this.InstPerClient.TabIndex = 0;
            this.InstPerClient.Text = "Number of Clients Per Instructor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label1.Location = new System.Drawing.Point(53, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(321, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of Confirmed Appointments Per Instructor";
            // 
            // ReturnLogin
            // 
            this.ReturnLogin.Location = new System.Drawing.Point(43, 482);
            this.ReturnLogin.Name = "ReturnLogin";
            this.ReturnLogin.Size = new System.Drawing.Size(180, 23);
            this.ReturnLogin.TabIndex = 2;
            this.ReturnLogin.Text = "Return";
            this.ReturnLogin.UseVisualStyleBackColor = true;
            this.ReturnLogin.Click += new System.EventHandler(this.ReturnLogin_Click);
            // 
            // ConfirmedAppointments
            // 
            chartArea2.Name = "ChartArea1";
            this.ConfirmedAppointments.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.ConfirmedAppointments.Legends.Add(legend2);
            this.ConfirmedAppointments.Location = new System.Drawing.Point(43, 91);
            this.ConfirmedAppointments.Name = "ConfirmedAppointments";
            this.ConfirmedAppointments.Size = new System.Drawing.Size(367, 282);
            this.ConfirmedAppointments.TabIndex = 3;
            this.ConfirmedAppointments.Text = "chart1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label2.Location = new System.Drawing.Point(466, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(324, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Number of Completed Appointments Per Instructor";
            // 
            // PaidAppointments
            // 
            chartArea3.Name = "ChartArea1";
            this.PaidAppointments.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.PaidAppointments.Legends.Add(legend3);
            this.PaidAppointments.Location = new System.Drawing.Point(855, 91);
            this.PaidAppointments.Name = "PaidAppointments";
            this.PaidAppointments.Size = new System.Drawing.Size(350, 282);
            this.PaidAppointments.TabIndex = 5;
            this.PaidAppointments.Text = "chart1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label3.Location = new System.Drawing.Point(852, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(285, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Number of Paid Appointments Per Instructor";
            // 
            // Statistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1231, 627);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PaidAppointments);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ConfirmedAppointments);
            this.Controls.Add(this.ReturnLogin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.InstPerClient);
            this.Name = "Statistics";
            this.Text = "Statistics";
            ((System.ComponentModel.ISupportInitialize)(this.InstPerClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmedAppointments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidAppointments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart InstPerClient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ReturnLogin;
        private System.Windows.Forms.DataVisualization.Charting.Chart ConfirmedAppointments;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataVisualization.Charting.Chart PaidAppointments;
        private System.Windows.Forms.Label label3;
    }
}